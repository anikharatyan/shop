<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public function register(Request $request){
        $data = $request->validate([
           'name' => 'required|max:225',
           'email' => 'required|email|unique:users',
           'password' =>'required|confirmed'
        ]);
        $data['password'] = bcrypt($request->password);
        $user = User::create($data);
        $token = $user->createToken('Register Token')->accessTokien;
        return response(['user' => $user, 'token' => $token]);
    }

    public function login(Request $request){
        $data = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);
        if(!auth()->attempt($data)){
            return response(['error_message' => 'Incorrect details. Please try again.']);
        }
        $token = auth()->user()->createToken('Login Token')->accessToken;
        return response(['user' => auth()->user(), 'token' => $token]);
    }

    public function logout(Request $request){
        $token = $request->user()->token();
        $token->revoke();
        return response(['message' => 'You have successfully logout!']);
    }
}
