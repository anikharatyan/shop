<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        return view('Products.index');
    }


    public function create()
    {
        return view('Products.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);

        Product::create($request->all());

        return redirect()->route('Products.index')
                         ->with('success','The product was created successfully!');
    }

    public function show(Product $product)
    {
        return view('Products.show');
    }

    public function edit(Product $product)
    {
        return view('Products.edit');
    }

    public function update(Request $request, Product $product)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        $product->update($request->all());

        return redirect()->route('Products.index')
                         ->with('success','The product was updated successfully!');
    }

    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('Products.index')
                         ->with('success','The product was deleted successfully!');
    }
}
